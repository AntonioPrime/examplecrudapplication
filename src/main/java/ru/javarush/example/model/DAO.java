package ru.javarush.example.model;

import ru.javarush.example.view.View;

public interface DAO {

    void updateView(String nameFilter);

    void setView(View view);

    void editDatabase(Action action, Entry entry);
}
