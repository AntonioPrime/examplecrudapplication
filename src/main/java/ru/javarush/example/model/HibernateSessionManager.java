package ru.javarush.example.model;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import java.io.Closeable;
import java.io.IOException;

public class HibernateSessionManager implements Closeable {

    private final SessionFactory sessionFactory = buildSessionFactory();

    private SessionFactory buildSessionFactory() {
        try {
            return new AnnotationConfiguration().
                    configure().addAnnotatedClass(User.class).buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void close() throws IOException {
        // Close caches and connection pools
        getSessionFactory().close();
    }
}