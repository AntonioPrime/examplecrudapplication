package ru.javarush.example.model;

import org.hibernate.Session;
import ru.javarush.example.view.View;

import java.util.*;

public class UsersDao implements DAO {
    private List<User> users;
    private HibernateSessionManager hibernateSessionManager;
    private View<User> view;
    private String nameFilter = "";

    public UsersDao(HibernateSessionManager manager) {
        hibernateSessionManager = manager;
        users = getAllUsersFromDB();
    }

    public void setView(View view) {
        this.view = view;
    }

    public List<User> getAllUsersFromDB() {
        Session session = null;
        List userList = null;
        try {
            session = hibernateSessionManager.getSessionFactory().openSession();
            userList = session.createCriteria(User.class).list();
        }
        catch (Exception ignored) {}
        finally {
            if (session!=null && session.isOpen()) {
                session.close();
            }
        }
        return userList;
    }
    @Override
    public void updateView(String nameFilter) {
        this.nameFilter = nameFilter;
        view.updateEntryList(generateFilteredList());
    }

    @Override
    public void editDatabase(Action action, Entry entry) {
        Session session = null;
        User user = (User) entry;
        try {
            session = hibernateSessionManager.getSessionFactory().openSession();
            session.beginTransaction();
            if (action.equals(Action.CREATE)) {
                session.save(user);
                users.add(user);
            }
            else if (action.equals(Action.UPDATE)) {
                session.update(user);
                int id = user.getId();
                updateUserInList(id, user);
            }
            else if (action.equals(Action.DELETE)) {
                session.delete(user);
                int id = user.getId();
                deleteUserFromListById(id);
            }
            session.getTransaction().commit();
            updateView(nameFilter);
        }
        catch (Exception ignored) {}
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
    private void deleteUserFromListById(int id) {
        for (Iterator<User> iterator = users.iterator(); iterator.hasNext(); ) {
            User user = iterator.next();
            if (user.getId()==id) {
                iterator.remove();
                break;
            }
        }
    }
    private void updateUserInList(int id, User user) {
        for (int i = 0; i < users.size(); i++) {
            User inner = users.get(i);
            if (inner.getId() == id) {
                users.set(i, user);
            }
        }
    }
    private List<User> generateFilteredList() {
        List<User> filteredList = new ArrayList<>();
        for (User user :
                users) {
            if (user.getName().toUpperCase().contains(nameFilter.toUpperCase())) {
                filteredList.add(user);
            }
        }
        return filteredList;
    }
}
