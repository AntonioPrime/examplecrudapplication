package ru.javarush.example.model;

public enum Action {
    CREATE,
    UPDATE,
    DELETE
}
