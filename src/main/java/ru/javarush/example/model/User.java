package ru.javarush.example.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "USER")
public class User implements Entry, Serializable, Cloneable{
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private int age;

    @Column(name = "isAdmin")
    private boolean isAdmin;

    @Column(name = "createdDate")
    private Date createdDate;

    public User() {
        createdDate = new Date();
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", isAdmin=" + isAdmin +
                ", createdDate=" + createdDate +
                '}';
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setProperty(String propName, Object value) {
        if (value instanceof Integer) {
            if (propName.equalsIgnoreCase("id")) id = (int) value;
            else if (propName.equalsIgnoreCase("age")) age = (int) value;
        }
        else if (value instanceof String) {
            if (propName.equalsIgnoreCase("name")) name = (String) value;
        }
        else if (value instanceof Boolean) {
            if (propName.equalsIgnoreCase("admin") || propName.equalsIgnoreCase("isAdmin"))
                isAdmin = (Boolean) value;
        }
        else if (value instanceof Date) {
            if (propName.equalsIgnoreCase("createdDate")) createdDate = (Date) value;
        }
    }
}