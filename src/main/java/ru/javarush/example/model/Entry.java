package ru.javarush.example.model;

public interface Entry {
    void setProperty(String propName, Object propValue);
}
