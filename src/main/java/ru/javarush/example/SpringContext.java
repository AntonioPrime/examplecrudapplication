package ru.javarush.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.javarush.example.model.UsersDao;
import ru.javarush.example.model.User;
import ru.javarush.example.view.Content;
import ru.javarush.example.view.Head;

public class SpringContext {
    private static final ApplicationContext context;

    static {
        context = new ClassPathXmlApplicationContext("bundle-context.xml");
    }
    public static User createEntry() {
        return (User) context.getBean("user");
    }
    public static UsersDao createDAO() {
        return (UsersDao) context.getBean("usersDAO");
    }
    public static Controller createController() {
        return (Controller) context.getBean("controller");
    }

    public static Content createContent() {
        return (Content) context.getBean("content");
    }

    public static Head createHead() {
        return (Head) context.getBean("head");
    }
}
