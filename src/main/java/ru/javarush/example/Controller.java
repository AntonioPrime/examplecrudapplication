package ru.javarush.example;

import ru.javarush.example.model.Action;
import ru.javarush.example.model.DAO;
import ru.javarush.example.model.Entry;
import ru.javarush.example.view.View;

import java.util.Map;

import static ru.javarush.example.SpringContext.createEntry;

public class Controller {
    private DAO dao;

    public Controller(DAO dao) {
        this.dao = dao;
    }
    private void setProperties(Entry entry, Map<String, Object> properties) {
        for (String propName :
                properties.keySet()) {
            entry.setProperty(propName, properties.get(propName));
        }
    }
    public void deleteEntry(Map<String, Object> properties) {
        Entry entry = createEntry();
        setProperties(entry, properties);
        dao.editDatabase(Action.DELETE, entry);
    }

    public void addEntry(Map<String, Object> properties) {
        Entry entry = createEntry();
        setProperties(entry, properties);
        dao.editDatabase(Action.CREATE, entry);
    }

    public void updateEntry(Map<String, Object> properties) {
        Entry entry = createEntry();
        setProperties(entry, properties);
        dao.editDatabase(Action.UPDATE, entry);
    }

    public void updateView(String nameFilter) {
        dao.updateView(nameFilter);
    }
    public void setView(View view) {
        dao.setView(view);
    }
}
