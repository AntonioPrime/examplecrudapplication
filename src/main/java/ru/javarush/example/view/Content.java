package ru.javarush.example.view;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import ru.javarush.example.Controller;
import ru.javarush.example.model.User;

import java.util.List;

public class Content extends Panel{
    private Controller controller;
    private Table table;
    private UpdateAndDeleteForm tableForm;

    public Content(Controller controller) {

        this.controller = controller;

        table = new Table();
        table.setSelectable(true);
        table.setSizeFull();
        table.setColumnReorderingAllowed(true);

        setWidth("700px");
        setHeight("800px");

        VerticalLayout panelContent = new VerticalLayout();
        panelContent.setSizeFull();
        panelContent.setMargin(true);
        setContent(panelContent);

        panelContent.addComponent(table);
        panelContent.setExpandRatio(table, 1.0f);
        tableForm = new UpdateAndDeleteForm(controller);
        tableForm.setVisible(false);
        panelContent.addComponent(tableForm);

        table.addValueChangeListener((Property.ValueChangeListener) event -> {
            Object selectedItemId = event.getProperty().getValue();
            if (selectedItemId != null) {
                tableForm.setItemDataSource(table.getItem(selectedItemId));
                if (! tableForm.isVisible()) {
                    table.setCurrentPageFirstItemId(selectedItemId);
                    tableForm.setVisible(true);
                }
            } else
                tableForm.setVisible(false);
        });

        table.setImmediate(true);
    }

    public void updateContent(List<User> usersList) {
        table.setContainerDataSource(new BeanItemContainer<>(User.class, usersList));
    }
}
