package ru.javarush.example.view;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.*;
import ru.javarush.example.Controller;

import java.util.HashMap;
import java.util.Map;

public class UpdateAndDeleteForm extends FormLayout {
            private Controller controller;

            TextField id = new TextField("ID");
            CheckBox admin = new CheckBox("admin");
            TextField name = new TextField("Name");
            TextField age = new TextField("Age");
            InlineDateField createdDate = new InlineDateField("Created Date");

            Button updUser = new Button("Update User");
            Button removeUser = new Button("Delete User");
            VerticalLayout textFieldsLayout = new VerticalLayout(admin, id, name, age);
            HorizontalLayout buttonLayout = new HorizontalLayout(updUser, removeUser);
            HorizontalLayout layout = new HorizontalLayout(textFieldsLayout, createdDate);
            VerticalLayout masterLayout = new VerticalLayout(layout, buttonLayout);

            public UpdateAndDeleteForm(Controller controller) {
                this.controller = controller;

                admin.setIcon(FontAwesome.KEY);
                id.setSizeFull();
                name.setSizeFull();
                age.setSizeFull();
                createdDate.setResolution(Resolution.SECOND);
                textFieldsLayout.setWidth(300, Unit.PIXELS);
                createdDate.setDateFormat("dd.MM.yyyy hh:mm:ss");
                addComponents(masterLayout);
            }


            public void setItemDataSource(Item item) {


                FieldGroup binder = new FieldGroup(item);
                binder.bindMemberFields(this);

                id.setReadOnly(true);

                removeUser.addClickListener(e -> {
                    new Notification(name.getValue() + " deleted").show(Page.getCurrent());
                    Map<String, Object> properties = new HashMap<>();
                    properties.put("id", Integer.parseInt(id.getValue()));
                    controller.deleteEntry(properties);
                });

                updUser.addClickListener(e -> {
                    try {
                        int checkedAge = Integer.parseInt(age.getValue());
                        new Notification(name.getValue() + " updated").show(Page.getCurrent());
                        Map<String, Object> properties = new HashMap<>();
                        properties.put("id", Integer.parseInt(id.getValue()));
                        properties.put("name", name.getValue());
                        properties.put("age", checkedAge);
                        properties.put("admin", admin.getValue());
                        properties.put("createdDate", createdDate.getValue());
                        controller.updateEntry(properties);
                    }
                    catch (NumberFormatException nE) {
                        new Notification("Age must be a Number").show(Page.getCurrent());
                    }
                });
            }
        }