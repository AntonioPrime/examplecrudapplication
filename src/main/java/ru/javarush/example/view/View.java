package ru.javarush.example.view;

import ru.javarush.example.model.Entry;

import java.util.List;

public interface View<T extends Entry> {
    void updateEntryList(List<T> entryList);
}
