package ru.javarush.example.view;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import ru.javarush.example.Controller;

import java.util.HashMap;
import java.util.Map;

public class Head extends VerticalLayout{
    private Controller controller;

    public Head(Controller controller) {


        this.controller = controller;

        final TextField nameFilter = new TextField();
        nameFilter.setCaption("Filter by Name:");
        nameFilter.setIcon(FontAwesome.SEARCH);
        nameFilter.addTextChangeListener(e -> {
            controller.updateView(e.getText());
        });
        final Button addButton = new Button("Add User");

        HorizontalLayout topMenu = new HorizontalLayout();
        topMenu.addComponents(nameFilter, addButton);
        topMenu.setComponentAlignment(addButton, Alignment.BOTTOM_LEFT);
        addComponent(topMenu);

        final HorizontalLayout createUserForm = new HorizontalLayout();
        createUserForm.setVisible(false);

        TextField newName = new TextField("Name");
        TextField newAge = new TextField("Age");
        CheckBox isAdmin = new CheckBox("Admin");
        isAdmin.setIcon(FontAwesome.KEY);
        Button createUserButton = new Button("Create User");
        createUserButton.addClickListener(e -> {
            if (newAge.isEmpty() || newName.isEmpty()) {
                new Notification("Cannot create User with empty fields").show(Page.getCurrent());
            }
            else {
                try {
                    int checkedAge = Integer.parseInt(newAge.getValue());
                    String name = newName.getValue();
                    Map<String, Object> properties = new HashMap<>();
                    properties.put("name", name);
                    properties.put("age", checkedAge);
                    properties.put("admin", isAdmin.getValue());
                    controller.addEntry(properties);
                    newAge.clear();
                    isAdmin.clear();
                    controller.updateView(name);
                    nameFilter.setValue(name);
                    new Notification(name + " created").show(Page.getCurrent());
                    newName.clear();
                    createUserForm.setVisible(false);
                } catch (NumberFormatException numE) {
                    new Notification("Age must be a Number").show(Page.getCurrent());
                    newAge.clear();
                }
            }
        });
        createUserForm.addComponents(newName, newAge, isAdmin, createUserButton);
        createUserForm.setComponentAlignment(isAdmin, Alignment.BOTTOM_LEFT);
        createUserForm.setComponentAlignment(createUserButton, Alignment.BOTTOM_RIGHT);

        addButton.addClickListener(e -> {
            if (createUserForm.isVisible()) createUserForm.setVisible(false);
            else createUserForm.setVisible(true);
        });

        addComponent(createUserForm);
    }
}
