package ru.javarush.example;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import ru.javarush.example.model.User;
import ru.javarush.example.view.Content;
import ru.javarush.example.view.Head;
import ru.javarush.example.view.View;

import java.util.List;

@Theme("mytheme")
@Widgetset("ru.javarush.example.MyAppWidgetset")
public class MyUI extends UI implements View<User>{
    private Controller controller = SpringContext.createController();
    private Content content;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        controller.setView(this);

        final VerticalLayout layout = new VerticalLayout();
        //HEAD
        Head topMenuAndCreateForm = SpringContext.createHead();
        //TABLE PANEL
        content = SpringContext.createContent();

        layout.addComponents(topMenuAndCreateForm, content);
        layout.setMargin(true);
        layout.setSpacing(true);
        
        setContent(layout);

        controller.updateView("");

    }

    public void updateEntryList(List<User> entryList) {
        content.updateContent(entryList);
    }


    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
